package com.wirax.vkr.documentmanager.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.wirax.vkr")
@ComponentScan(basePackages = "com.wirax.vkr")
public class AppConfig {
}
