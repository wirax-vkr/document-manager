package com.wirax.vkr.documentmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "vkr_statement")
@NoArgsConstructor
@AllArgsConstructor
public class Statement extends Document {
    private UUID approve;
    private OffsetDateTime fromDate;
    private OffsetDateTime toDate;
}
