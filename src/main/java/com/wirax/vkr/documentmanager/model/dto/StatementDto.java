package com.wirax.vkr.documentmanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatementDto extends DocumentDto {
    private UUID approve;
    private OffsetDateTime from;
    private OffsetDateTime to;
}
