package com.wirax.vkr.documentmanager.model.dto;

import com.wirax.vkr.documentmanager.model.Task;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto extends DocumentDto {
    private String technicalDescription;
    private List<String> diagramLinks;
    private OffsetDateTime dueDate;
    private Task.TaskType type;
    private UUID resolvedBy;
    private List<String> comments;
    private UUID project;
}
