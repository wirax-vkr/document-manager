package com.wirax.vkr.documentmanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Task extends Document {
    private String technicalDescription;
    @ElementCollection
    private List<String> diagramLinks;
    private OffsetDateTime dueDate;
    private TaskType type;
    private UUID resolvedBy;
    @ElementCollection
    private List<String> comments;
    private UUID project;


    public enum TaskType {
        BUSINESS_TASK, SOFTWARE_DEVELOPMENT, QUALITY_TASK, RELEASE_TASK
    }
}
