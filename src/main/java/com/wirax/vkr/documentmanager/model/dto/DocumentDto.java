package com.wirax.vkr.documentmanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class DocumentDto {
    private UUID id;
    private String description;
    private UUID assignee;
    private UUID createdBy;
    private UUID closedBy;
}
