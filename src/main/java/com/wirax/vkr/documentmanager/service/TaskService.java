package com.wirax.vkr.documentmanager.service;

import com.wirax.vkr.documentmanager.model.Task;
import com.wirax.vkr.documentmanager.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TaskService {
    private final TaskRepository repository;

    @Autowired
    public TaskService(TaskRepository repository) {
        this.repository = repository;
    }

    public Task create(Task technicalSpecialist) {
        return repository.save(technicalSpecialist);
    }

    public Task update(UUID id, Task user) {
        if(id != user.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return repository.save(user);
    }

    public List<Task> findAll() {
        return repository.findAll();
    }

    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
