package com.wirax.vkr.documentmanager.service;

import com.wirax.vkr.documentmanager.model.Statement;
import com.wirax.vkr.documentmanager.repository.StatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StatementService {
    private final StatementRepository repository;

    @Autowired
    public StatementService(StatementRepository repository) {
        this.repository = repository;
    }

    public Statement create(Statement document) {
        return repository.save(document);
    }

    public Statement update(UUID id, Statement document) {
        if(id != document.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return repository.save(document);
    }

    public List<Statement> findAll() {
        return repository.findAll();
    }

    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
