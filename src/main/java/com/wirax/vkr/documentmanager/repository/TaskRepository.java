package com.wirax.vkr.documentmanager.repository;

import com.wirax.vkr.documentmanager.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TaskRepository extends JpaRepository<Task, UUID> {
}
