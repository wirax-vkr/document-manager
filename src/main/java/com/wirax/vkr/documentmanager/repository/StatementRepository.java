package com.wirax.vkr.documentmanager.repository;

import com.wirax.vkr.documentmanager.model.Statement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface StatementRepository extends JpaRepository<Statement, UUID> {
}
