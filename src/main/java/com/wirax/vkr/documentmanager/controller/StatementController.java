package com.wirax.vkr.documentmanager.controller;

import com.wirax.vkr.documentmanager.mapper.StatementMapper;
import com.wirax.vkr.documentmanager.mapper.StatementMapperImpl;
import com.wirax.vkr.documentmanager.model.dto.StatementDto;
import com.wirax.vkr.documentmanager.service.StatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class StatementController {
    public final StatementService service;
    private StatementMapper mapper = new StatementMapperImpl();

    @Autowired
    public StatementController(StatementService service) {
        this.service = service;
    }

    @PostMapping("/statement")
    public StatementDto create(@RequestBody StatementDto statement) {
        return mapper.toDto(service.create(mapper.toEntity(statement)));
    }

    @PutMapping("/statement")
    public StatementDto update(@RequestParam UUID id, @RequestBody StatementDto user) {
        return mapper.toDto(service.update(id, mapper.toEntity(user)));
    }

    @GetMapping("/statement")
    public List<StatementDto> findAll() {
        return service.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/statement")
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
