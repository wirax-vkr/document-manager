package com.wirax.vkr.documentmanager.controller;

import com.wirax.vkr.documentmanager.mapper.TaskMapper;
import com.wirax.vkr.documentmanager.mapper.TaskMapperImpl;
import com.wirax.vkr.documentmanager.model.dto.TaskDto;
import com.wirax.vkr.documentmanager.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class TaskController {
    public final TaskService service;
    private TaskMapper mapper = new TaskMapperImpl();

    @Autowired
    public TaskController(TaskService service) {
        this.service = service;
    }

    @PostMapping("/task")
    public TaskDto create(@RequestBody TaskDto user) {
        return mapper.toDto(service.create(mapper.toEntity(user)));
    }

    @PutMapping("/task")
    public TaskDto update(@RequestParam UUID id, @RequestBody TaskDto user) {
        return mapper.toDto(service.update(id, mapper.toEntity(user)));
    }

    @GetMapping("/task")
    public List<TaskDto> findAll() {
        return service.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/task")
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
