package com.wirax.vkr.documentmanager.mapper;

import com.wirax.vkr.documentmanager.model.Statement;
import com.wirax.vkr.documentmanager.model.dto.StatementDto;
import org.mapstruct.Mapper;

@Mapper
public interface StatementMapper {
    Statement toEntity(StatementDto dto);

    StatementDto toDto(Statement entity);
}
