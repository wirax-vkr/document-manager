package com.wirax.vkr.documentmanager.mapper;

import com.wirax.vkr.documentmanager.model.Task;
import com.wirax.vkr.documentmanager.model.dto.TaskDto;
import org.mapstruct.Mapper;

@Mapper
public interface TaskMapper {
    Task toEntity(TaskDto dto);

    TaskDto toDto(Task entity);
}
